<?php

## Syntax:
##     php build.php { <platform> | all }
## ('all' builds for all available platforms)
##
## This script will create build/ and build/<platform> if needed, then will
## copy files from src/ and src/platform/<platform> into build/<platform>/.
## The extension can then be loaded from build/<platform>.

## No platform specified.
if ($argc < 2) {
	echo "Syntax is:\n";
	echo "\tphp build.php { <platform> | all }\n";
}

## Determine path to repository root.
$root = dirname(__FILE__);

## Determine what platforms are available.
$platforms = array_filter(scandir("{$root}/src/platform/"), function ($str) {
	return strncmp($str, ".", 1);
});

## No platform specified, or specified platform does not exist.
## NOTE: specifying 'all' will build for all platforms.
if ($argc < 2 || !(in_array($argv[1], $platforms) || $argv[1] == "all")) {
	echo "Available platforms are:\n";
	foreach ($platforms as $platform)
		echo "\t{$platform}\n";
	die;
}

## Build for specified platform (or for all platforms, if 'all' is given).
if ($argv[1] == "all") {
	foreach ($platforms as $platform) {
		build_for_platform($platform);
	}
} else {
	build_for_platform($argv[1]);
}

function build_for_platform($platform) {
	global $root;
	echo "Building for: {$platform}\n";

	$command = "cd {$root}; ";
	$command .= "mkdir -p build/{$platform}; ";
	$command .= "rm -rf build/{$platform}/*; ";
	$command .= "cp -R src/{*.js,*.html,*.css,fonts,images} build/{$platform}/; ";
	$command .= "cp -R src/platform/{$platform}/* build/{$platform}/";

	`bash -c "{$command}"`;
}

?>
