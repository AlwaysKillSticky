function completeInitialization() {
	// Set the default background color of the action icon badge.
	chrome.browserAction.setBadgeBackgroundColor({
		color: "#0071b3"
	});

	/*	Toggle the toolbar icon based on whether stickies are, or are not, set to
		be killed on the current tab, or update the badge with # of stickies killed.

		This listener receives a message from the content script (contentScript.js);
		the content script actually determines what the settings for the current
		tab are.
		*/
	chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
		if (typeof request.killingStickies != "undefined") {
			let icons = request.killingStickies ? 
						AKS.actionIcons.on :
						AKS.actionIcons.off;
			chrome.browserAction.setIcon({
				path: icons,
				tabId: request.tabID || sender.tab.id
			});
		} else if (typeof request.newBadgeText != "undefined") {
			chrome.browserAction.setBadgeText({
				tabId: sender.tab.id,
				text: request.newBadgeText
			});
		}
	});
}

function initialize() {
	window.AKS = { };

	var xhr = new XMLHttpRequest();
	xhr.onload = () => {
		AKS.actionIcons = JSON.parse(xhr.response);
		completeInitialization();
	};
	xhr.open('GET', chrome.extension.getURL('action_icons.json'), true);
	xhr.send();
}

initialize();


