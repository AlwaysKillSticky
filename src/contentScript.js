/***********/
/* HELPERS */
/***********/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*	Given the result of a query to storage (for the matching and exclusion 
	pattern lists, and the current mode - whitelist or blacklist), this function
	determines whether stickies should be killed on the currently loaded page.

	Stickies are killed if the following is true:
	
	(blacklist mode is active, AND 
	 at least one matching pattern matches the URL of the current page, AND
	 zero exclusion patterns match the URL of the current page)
		 OR 
	 (whitelist mode is active, AND
	  zero exclusion matters match the URL of the current page)
	*/
function checkForShouldKillSticky(result) {
// 	console.log("checkForShouldKillSticky");
	var shouldKillSticky = false;

	/*	If whitelist mode is active, then the matching patterns list is treated
		as containing a single pattern which will match all possible URLs.
		Patterns that are empty strings (i.e., blank lines in the patterns 
		lists) are ignored, as are any patterns that begin with a pound sign (#)
		(this allows for comments in the patterns lists). */
	let matchingPatterns = result.mode == "whitelist" ?
						   [ ".*" ] :
						   (typeof result.matchingPatterns != "undefined" ?
							result.matchingPatterns.split("\n") :
							[ ]);
	for (let pattern of matchingPatterns) {
		if (pattern && 
			!pattern.hasPrefix("#") &&
			location.href.match(new RegExp(pattern))) {
			shouldKillSticky = true;
			break;
		}
	}
	let exclusionPatterns = typeof result.exclusionPatterns != "undefined" ?
							result.exclusionPatterns.split("\n") :
							[ ];
	for (let pattern of exclusionPatterns) {
		if (pattern && 
			!pattern.hasPrefix("#") &&
			location.href.match(new RegExp(pattern))) {
			shouldKillSticky = false;
			break;
		}
	}
	
	return shouldKillSticky;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*	This is the code that actually does the sticky killing! It simply selects
	all elements whose ‘position’ CSS property has a computed value of either
	‘sticky’ or ‘fixed’, and removes those elements.
	*/
function killSticky(root) {
// 	console.log("killSticky");
	root = root || document.querySelector("body");
	if (killStickyIfNeeded(root) == false)
		killAllStickiesWithin(root);

	// Compensate for full-screen paywalls.
	if (root == document.body)
		restoreScrollability();
}

function killAllStickiesWithin(root) {
	root.querySelectorAll('*').forEach(element => {
		if (killStickyIfNeeded(element) == false && element.shadowRoot != null)
			killAllStickiesWithin(element.shadowRoot);
	});
}

function killStickyIfNeeded(element) {
// 	console.log("killStickyIfNeeded");
	var position = getComputedStyle(element).position;
	if (position === 'fixed' || position === 'sticky') {
		// uBlock exception.
		if (element.tagName == "IFRAME" && 
			element.parentElement.tagName == "HTML")
			return;

		// Kill the sticky.
		console.log("Killing sticky!");
		element.remove();

		// Increment the stickies killed count, and update icon badge.
		incrementKilledStickiesCount();
		
		return true;
	} else {
		var didKillSticky = false;

		// The ::before pseudo-element might be sticky on its own.
		position = getComputedStyle(element, "::before").position;
		if (position === 'fixed' || position === 'sticky') {
			// Kill the sticky.
			console.log("Killing sticky!");
			killStickyPseudoElement(element, true);

			// Increment the stickies killed count, and update icon badge.
			incrementKilledStickiesCount();

			didKillSticky = true;
		}

		// The ::after pseudo-element, too, might be sticky on its own.
		position = getComputedStyle(element, "::after").position;
		if (position === 'fixed' || position === 'sticky') {
			// Kill the sticky.
			console.log("Killing sticky!");
			killStickyPseudoElement(element, false);

			// Increment the stickies killed count, and update icon badge.
			incrementKilledStickiesCount();

			didKillSticky = true;
		}

		return false;
	}
}

function killStickyPseudoElement(element, before) {
// 	console.log("killStickyPseudoElement");
	if (!AKS.pseudoElementKillingStyleBlock) {
		document.querySelector("head").insertAdjacentHTML("beforeend", "<style id='always-kill-sticky'></style>");
		AKS.pseudoElementKillingStyleBlock = document.querySelector("style#always-kill-sticky");
	}
	
	var id = element.id;
	if (id == "")
		element.id = "always-kill-sticky-killed-element-" + (++AKS.stickyElementIDs);
		
	let selector = `${element.tagName}#${element.id}::${(before ? 'before' : 'after')}`;
	AKS.pseudoElementKillingStyleBlock.innerHTML = AKS.pseudoElementKillingStyleBlock.innerHTML + `${selector} { content: none; }\n`;
}

function incrementKilledStickiesCount() {
// 	console.log("incrementKilledStickiesCount");
	if (!AKS.hideKillCount) {
		AKS.stickiesKilled++;
		chrome.runtime.sendMessage({ newBadgeText: "" + AKS.stickiesKilled });
	}
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*	Full-screen paywalls not only bring up sticky elements, they also make
	the page un-scrollable. This ensures that the page remains scrollable.
	*/
function restoreScrollability() {
// 	console.log("restoreScrollability");
	document.querySelectorAll("html, body").forEach(container => {
		container.style.setProperty("overflow-y", "auto", "important");
	});
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*	Some websites add sticky elements after loading, or make existing elements
	sticky (either on a timer, or based on user actions). CONSTANT VIGILANCE
	counteracts this behavior, by watching the DOM for mutations, and, if
	necessary, killing any newly-formed stickies.
	*/
function startConstantVigilance() {
// 	console.log("startConstantVigilance");
	var observer;
	var starter = function() {
		observer.observe(document.querySelector("html"), {
			attributes: true,
			childList: true,
			subtree: true
		});
	};

	observer = new MutationObserver((mutationsList, observer) => {
		// Workaround for Firefox performance bug. Firefox does not properly
		// coalesce mutations into the mutationsList, instead calling the
		// callback potentially thousands of times. Therefore we must
		// disconnect the observer, wait for the main thread to become idle,
		// and then restart it.
		observer.disconnect();
		window.requestIdleCallback(() => {
			killSticky();
			starter();
		}, {timeout: 1000});
	});

	console.log("Commencing vigilance for stickies!");
	starter();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*	Immediately kill all stickies, then commence constant vigilance.
	*/
function beginKillingStickies() {
// 	console.log("beginKillingStickies");
	if (!window.AKS) {
		window.AKS = { stickiesKilled: 0 };
	}
	else {
		window.AKS.stickiesKilled = 0;
	}

	window.requestIdleCallback(() => {
		killSticky();
		startConstantVigilance();
	}, {timeout: 1000});
}

/******************/
/* INITIALIZATION */
/******************/

function initialize() {
// 	console.log("initialize");
	chrome.storage.local.get([ "matchingPatterns", "exclusionPatterns", "mode", "hideKillCount" ], (result) => {
		let shouldKillSticky = checkForShouldKillSticky(result);
		updateIcon(shouldKillSticky);
		const hideKillCount = result.hideKillCount ? true : false;
		if (!window.AKS){
			window.AKS = {hideKillCount: hideKillCount};
		}
		else {
			window.AKS.hideKillCount = hideKillCount;
		}
		if (shouldKillSticky) {
			if (document.readyState == "loading") {
				document.addEventListener("DOMContentLoaded", beginKillingStickies);
			} else {
				beginKillingStickies();
			}
		}
	});
}
initialize();
