# AlwaysKillSticky

A browser extension that automatically gets rid of sticky elements on websites you visit.

## Installation

### Chrome/Vivaldi

* Install via the [Chrome web store](https://chrome.google.com/webstore/detail/alwayskillsticky/oolchklbojaobiipbmcnlgacfgficiig).

### Firefox

* Install via [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/alwayskillsticky/).

### Opera

* [Download the latest release](https://wiki.obormot.net/Main/AlwaysKillSticky), or build from source (see below).
* Unzip the package.
* Go to `about://extensions` in the URL bar.
* Enable Developer Mode by clicking the button (Opera).
* Click the “Load Unpacked Extension” button.
* Select the AlwaysKillSticky-chrome folder.

### Building from source

You can build the extension from source, using the provided `build.php` script.

### Post-installation

If you follow the above instructions, AlwaysKillSticky should now be installed!

Stickies will not be killed yet; you must now add some URLs to the **matching patterns** list (or, switch to **whitelist mode**); see **Usage**, below.

## Usage

Open the AlwaysKillSticky options window (either by clicking on the AlwaysKillSticky icon in your toolbar, or by going to your Extensions page, clicking “Details”, then clicking “Extension options”).

AlwaysKillSticky can work in either two modes: **blacklist mode** and **whitelist mode**.

In **blacklist mode** (the default), stickies will *only* be killed if:

* the page URL matches one of the patterns in the **matching patterns** list, *and*;
* the page does *not* match any of the patterns in the **exclusion patterns** list.

Conversely, in **whitelist mode**, the matching patterns list is *ignored*. Stickies are *always* killed, *unless* the page matches one of the patterns in the exclusion patterns list.

You can switch between blacklist mode and whitelist mode at any time.

## License

This extension is released under the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.
