<?php

$root = preg_replace('/\/[^\/]+$/', '', dirname(__FILE__));
$directories = [
	"{$root}/src/"
];
$files = [ 
	"options.js",
	"popup.js",
	"options.css",
	"popup.css",
	"options.html",
	"popup.html",
];
$additional_files = [
];
$characters = [ ];

function process_file($filename) {
	if (!file_exists($filename))
		return;

	global $characters;
	
	$contents = file_get_contents($filename);

	preg_match_all('/&#x(.{4})/', $contents, $matches);	
	$characters = array_merge($characters, $matches[1]);

	preg_match_all('/\\\(F.{3})/', $contents, $matches);
	$characters = array_merge($characters, $matches[1]);
}

foreach ($directories as $directory) {
	foreach ($files as $file) {
		process_file($directory.$file);
	}
}
foreach ($additional_files as $file) {
	process_file($file);
}

foreach ($characters as $key => $value) {
	$characters[$key] = strtoupper($value);
}
$characters = array_unique($characters);
sort($characters);
echo implode(",",$characters);
echo "\n";

?>