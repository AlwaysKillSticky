# Convenience scripts

This folder contains some convenience scripts. They’re mostly for me, but may be useful for anyone who wants to contribute to developing AlwaysKillSticky, so they’re documented here for completeness.

## `package.php`

Will create `.tar.gz` and `.zip` archives in the `release/` directory (creating it if needed) of the built extension for each platform (in `build/`). The archives will be named according to platform and version (read from `manifest.json` in each platform’s build directory).

Passing the `-b` flag will run `build.php all` first, before packaging.

## `compute_FontAwesome_subset.php`

Will output the Unicode code values for all Font Awesome glyphs used in the extension. (For use with font subsetting tools.)

## `generator_config.txt`

When re-subsetting Font Awesome for packaging with AlwaysKillSticky, upload this file to the font generator tool at Font Squirrel (replacing the list of Unicode code values with the output of `compute_FontAwesome_subset.php`) in order to automatically set the right values for all the various generator options.

(This need only be done if adding or removing Font Awesome characters, e.g. when adding new buttons or other UI elements.)