<?php

$root = preg_replace('/\/[^\/]+$/', '', dirname(__FILE__));

if ($argv[1] == "-b") {
	$argv[1] = "all";
	include_once("{$root}/build.php");
}
	
$platforms = array_filter(scandir("{$root}/src/platform/"), function ($str) {
	return strncmp($str, ".", 1);
});

foreach ($platforms as $platform) {
	echo "Packaging for: {$platform}\n";

	$manifest = JSON_decode(file_get_contents("{$root}/build/{$platform}/manifest.json"), true);
	$version = $manifest["version"];

	$command = "cd {$root}/build/{$platform}; ";
	$command .= "find . -type f -name '.DS_Store' -delete; ";
	$command .= "mkdir -p {$root}/release; ";
	$command .= "tar czf {$root}/release/AlwaysKillSticky-{$platform}-{$version}.tar.gz *; ";
	$command .= "zip -r -FS {$root}/release/AlwaysKillSticky-{$platform}-{$version}.zip *";
	`{$command}`;
}

?>